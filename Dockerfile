FROM nodered/node-red
RUN npm install node-red-node-xmpp node-red-node-mysql node-red-dashboard node-red-contrib-modbus node-red-contrib-moving-average
# node-red-contrib-boolean-logic-ultimate